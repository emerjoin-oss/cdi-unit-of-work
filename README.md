# CDI UnitOfWork Context
Well, this is a CDI extension that provides a new Context to CDI: Unit-Of-Work. 

## Maven coordinates

### Artifact
```xml
<dependencies>
    <dependency>
        <groupId>org.emerjoin.cdi</groupId>
        <artifactId>cdi-unit-of-work</artifactId>
        <version>1.0.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```

### Repository
Emerjoin's OSS Repository
```xml
<repositories>
    <repository>
         <id>emerjoin</id>
         <name>oss</name>
         <url>https://pkg.emerjoin.org/oss</url>
    </repository>
</repositories>
```

## User Manual
There are three things you need to learn:
* How to activate the API
* How to create Unit-of-Work scoped beans
* How to start a Unit-of-Work context
* How to demand an Isolated Unit-of-Work context

### Activating the API
Just declare the interceptor class in your __beans.xml__:
```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://xmlns.jcp.org/xml/ns/javaee"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/beans_2_0.xsd" bean-discovery-mode="all" version="2.0">
    <interceptors>
        <class>org.emerjoin.cdi.context.unitofwork.UnitOfWorkInterceptor</class>
    </interceptors>
</beans>
```

### Creating Unit-of-work Scoped beans
You just need to annotate the bean class or producer method with __@UnitOfWorkScoped__.


#### Class
```java
@UnitOfWorkScoped
public class SomeObject {
    
    //properties and methods   

}
```

#### Producer
```java
public class ProducerBean {
    
    //properties and methods  
 
    @Produces @UnitOfWorkScoped
    public SomeObject produceBean(){
        //TODO: Create the object
    }

}
```

### Starting the Unit-of-Work context
The Unit-of-Work context can be started automatically or manually.
When started automatically it is also ended automatically. When started manually it must as well be ended manually.

#### Starting Unit-of-Work context automatically
This is as easy as annotating a **method** of a bean or a **bean type** with **@UnitOfWork**.

##### Method annotation
See the code sample below:
```java
@ApplicationScoped
public class SomeBean {
    
    
    @UnitOfWork
    public void method1(){
        //TODO: do something
    }

    public void method2(){
        //TODO: do something
    }

}
```
When the annotation is placed on a bean's **method**, it will cause a new Unit-of-Work context to be started when invoking the method (in case there is no active Unit-of-Work context).
The Context will only last during the method execution. Once the method ends, the context ends as well, unless the context was started by an upper method in the call stack or was started manually.

##### Bean type annotation
See the code sample below:
```java
@UnitOfWork
@ApplicationScoped
public class SomeBean {
    //Methods may come in here
}
```
When the annotation is pace on a bean type, all the public methods of the bean type will behave as if they were annotated with **@UnitOfWork**. 

##### Starting Unit-of-Work context manually
Starting the context is as simple as:
```java
UnitOfWorkContext.startNew();
```
Ending the current context is simple as well:
```java
UnitOfWorkContext.endCurrent();
```
You typically only want to manage the context manually in unit testing, in not very common scenarios.


### Isolated Context
By default, the **UnitOfWork** annotation will only start a new context if there is no active context. 
But sometimes you want a context to be fully isolated from others, not sharing it's **@UnitOfWorkScoped** beans, nor reusing 
beans from the active context.

How do you achieve that? It is as simple as follows:
```java
@UnitOfWork(isolate=true)
```

