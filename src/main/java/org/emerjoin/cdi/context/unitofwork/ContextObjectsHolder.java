package org.emerjoin.cdi.context.unitofwork;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ContextObjectsHolder {

    private Map<String,ScopedInstance> map = new HashMap<>();

    void put(ScopedInstance scopedInstance){
        String clazzName = scopedInstance.getBean().getBeanClass().getCanonicalName();
        map.put(clazzName,scopedInstance);
    }

    Optional<ScopedInstance> get(Class clazz){
        return Optional.ofNullable(map.get(clazz.
                getCanonicalName()));
    }

    void destroyAll(){
        for(ScopedInstance instance: map.values())
            instance.getBean().destroy(instance.getObject(),instance
                    .getContext());
        map.clear();
    }

}
