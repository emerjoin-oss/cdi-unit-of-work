package org.emerjoin.cdi.context.unitofwork;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;

public class ScopedInstance<T> {

    private Bean<T> bean;
    private CreationalContext<T> context;
    private T object;

    ScopedInstance(Bean<T> bean, CreationalContext<T> context, T object){
        this.bean = bean;
        this.context = context;
        this.object = object;
    }

    Bean<T> getBean() {
        return bean;
    }

    CreationalContext<T> getContext() {
        return context;
    }

    T getObject() {
        return object;
    }
}
