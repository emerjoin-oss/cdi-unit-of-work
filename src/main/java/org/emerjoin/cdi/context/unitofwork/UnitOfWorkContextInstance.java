package org.emerjoin.cdi.context.unitofwork;

import java.util.Optional;
import java.util.UUID;

public class UnitOfWorkContextInstance {

    private String id;
    private ContextObjectsHolder holder = new ContextObjectsHolder();

    UnitOfWorkContextInstance(){
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    void destroy(){
        this.holder.destroyAll();
    }


    Optional<ScopedInstance> getBeanInstance(Class beanClazz){
        return this.holder.get(beanClazz);
    }

    void putBeanInstance(ScopedInstance scopedInstance){

        this.holder.put(scopedInstance);

    }


}
