package org.emerjoin.cdi.context.unitofwork;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Priority;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.lang.reflect.Method;
import java.util.Optional;

@UnitOfWork
@Interceptor
@Priority(Interceptor.Priority.PLATFORM_BEFORE+10)
public class UnitOfWorkInterceptor {

    private static final Logger LOGGER = LogManager.getLogger(UnitOfWorkInterceptor.class);

    private String toString(Method method){
        return String.format("%s.%s(..)",method.getDeclaringClass().getCanonicalName(),method
                .getName());
    }

    @AroundInvoke
    public Object intercept(InvocationContext ctx) throws Exception {
        Method method = ctx.getMethod();
        LOGGER.info("Intercepting: "+toString(method));
        UnitOfWork boundary = ctx.getMethod().getAnnotation(UnitOfWork.class);
        if(boundary==null) {
            boundary = ctx.getMethod().getDeclaringClass().getAnnotation(UnitOfWork.class);
            if(boundary!=null)
                LOGGER.debug("Binding annotation found on method declaring class");
            else LOGGER.debug("Binding annotation not found. Assuming default: isolation=false");
        }else LOGGER.debug("Binding annotation found on intercepted method");
        ThreadState threadState = ThreadState.current();
        boolean started = false;
        try {
            if (!threadState.isContextActive()) {
                LOGGER.info("Starting a new Unit-of-Work Context");
                UnitOfWorkContextInstance contextInstance = threadState.startNewContext();
                LOGGER.info("Context started successfully [" + contextInstance.getId() + "]");
                started = true;
            } else {
                if (boundary!=null&&boundary.isolate()) {
                    LOGGER.info("Starting a new isolated Unit-of-Work Context");
                    UnitOfWorkContextInstance contextInstance = threadState.startNewContext();
                    LOGGER.info("Isolated Context started successfully [" + contextInstance.getId() + "]");
                    started = true;
                } else LOGGER.info("Using Active Unit-of-Work Context: [{}]",threadState.currentContext().get().
                        getId());
            }
            return ctx.proceed();
        }finally {
            if(started) {
                if(threadState.isContextActive()) {
                    Optional<UnitOfWorkContextInstance> optionalContextInstance = threadState.currentContext();
                    UnitOfWorkContextInstance contextInstance = optionalContextInstance.get();
                    LOGGER.info("Ending current Unit-of-Work Context [" + contextInstance.getId() + "]");
                    threadState.endCurrentContext();
                }else{
                    LOGGER.warn("Context had already been ended. This is a bad sign. You are not supposed to stop contexts you haven't started");
                }
            }
        }
    }

}
