package org.emerjoin.cdi.context.unitofwork;

import java.util.Optional;
import java.util.Stack;

public class ThreadState {

    private static final ThreadLocal<ThreadState> STATE_THREAD_LOCAL = new ThreadLocal<>();

    private Stack<UnitOfWorkContextInstance> contextInstances = new Stack<>();

    private ThreadState(){

    }

    public boolean isContextActive(){

        return !contextInstances.isEmpty();

    }

    Optional<UnitOfWorkContextInstance> currentContext(){
        if(contextInstances.isEmpty())
            return Optional.empty();
        return Optional.of(contextInstances
                .peek());
    }

    int countContexts(){
        return contextInstances.size();
    }

    UnitOfWorkContextInstance startNewContext(){
        UnitOfWorkContextInstance contextInstance = new UnitOfWorkContextInstance();
        contextInstances.add(contextInstance);
        return contextInstance;
    }

    void endCurrentContext(){
        if(contextInstances.isEmpty())
            return;
        UnitOfWorkContextInstance instance = contextInstances.pop();
        instance.destroy();
    }

    static ThreadState current(){
        ThreadState state = STATE_THREAD_LOCAL.get();
        if(state==null){
            state = new ThreadState();
            STATE_THREAD_LOCAL.set(state);
        }
        return state;
    }

}
