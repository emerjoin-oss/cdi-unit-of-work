package org.emerjoin.cdi.context.unitofwork;

import javax.enterprise.context.ContextNotActiveException;
import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.Contextual;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import java.lang.annotation.Annotation;
import java.util.Optional;

public class UnitOfWorkContext implements Context {

    UnitOfWorkContext(){

    }

    @Override
    public Class<? extends Annotation> getScope() {
        return UnitOfWorkScoped.class;
    }

    @Override
    public <T> T get(Contextual<T> contextual, CreationalContext<T> creationalContext) {
        if(!ThreadState.current().isContextActive())
            return null;
        Bean bean = (Bean) contextual;
        Optional<Object> objectOptional = getBean(bean.getBeanClass());
        if(!objectOptional.isPresent()){
            T t = (T) bean.create(creationalContext);
            ScopedInstance instance = new ScopedInstance(bean,creationalContext,t);
            ThreadState.current().currentContext().get().putBeanInstance(instance);
            return t;
        }
        return (T) objectOptional.get();

    }

    @Override
    public <T> T get(Contextual<T> contextual) {
        Bean bean = (Bean) contextual;
        Optional<Object> objectOptional = getBean(bean.getBeanClass());
        if(!objectOptional.isPresent())
            return null;
        return (T) objectOptional.get();
    }

    private Optional<Object> getBean(Class clazz){
        ThreadState threadState = ThreadState.current();
        Optional<UnitOfWorkContextInstance> optionalContextInstance = threadState.currentContext();
        if(!optionalContextInstance.isPresent()) {
            throw new ContextNotActiveException();
        }
        Optional<ScopedInstance> scopedInstance = optionalContextInstance.get().getBeanInstance(clazz);
        if(!scopedInstance.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(scopedInstance.get().
                getObject());
    }

    @Override
    public boolean isActive() {
        return ThreadState.current().isContextActive();
    }



    public static Optional<UnitOfWorkContextInstance> startNewIfNotActive(){
        if(!isCurrentlyActive())
            return Optional.of(startNew());
        return Optional.empty();
    }

    public static boolean isCurrentlyActive(){

        return ThreadState.current().isContextActive();

    }

    public static UnitOfWorkContextInstance startNew(){
        return ThreadState.current()
                .startNewContext();
    }


    public static void endIfActive(){
        if(isCurrentlyActive())
            endCurrent();
    }

    public static void endCurrent(){
        mustBeActive();
        ThreadState.current().endCurrentContext();
    }


    public static Optional<UnitOfWorkContextInstance> current(){
        return ThreadState.current().currentContext();
    }

    private static void mustBeActive(){
        if(!isCurrentlyActive())
            throw new ContextNotActiveException();
    }

}
