import org.emerjoin.cdi.context.unitofwork.UnitOfWork;
import org.emerjoin.cdi.context.unitofwork.UnitOfWorkContext;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class BarBean {

    @UnitOfWork(isolate = true)
    public String executeIsolatedContext(){
        return UnitOfWorkContext.current().get()
                .getId();
    }

    @UnitOfWork()
    public String executeSameContext(){
        return UnitOfWorkContext.current().get()
                .getId();
    }

}
