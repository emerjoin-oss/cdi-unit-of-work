import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.emerjoin.cdi.context.unitofwork.UnitOfWorkContext;
import org.emerjoin.cdi.context.unitofwork.UnitOfWorkContextInstance;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.context.ContextNotActiveException;
import javax.inject.Inject;
import static org.junit.Assert.*;

@RunWith(CdiTestRunner.class)
public class UnitOfWorkContextTest {

    @Inject
    private FooBean foo;

    @Inject
    private BarBean bar;

    @Test
    public void context_must_be_deemed_active(){

        assertTrue(foo.isContextActive());

    }

    @Test
    public void active_context_must_be_used_whenever_unit_of_work_isolated_flag_is_false(){
        UnitOfWorkContextInstance contextInstance = UnitOfWorkContext.startNew();
        String barContextId = bar.executeSameContext();
        UnitOfWorkContext.endCurrent();
        assertEquals(contextInstance.getId(),barContextId);
    }


    @Test
    public void separate_context_must_be_used_when_isolate_flag_is_true(){
        UnitOfWorkContextInstance contextInstance = UnitOfWorkContext.startNew();
        String barContextId = bar.executeIsolatedContext();
        UnitOfWorkContext.endCurrent();
        assertNotEquals(contextInstance.getId(),barContextId);
    }


    @Test(expected = ContextNotActiveException.class)
    public void context_not_active_exception_must_be_thrown_when_trying_to_end_current_context_while_there_is_no_active_context(){
        UnitOfWorkContext.endCurrent();;
    }



}
