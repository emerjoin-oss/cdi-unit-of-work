import org.emerjoin.cdi.context.unitofwork.UnitOfWork;
import org.emerjoin.cdi.context.unitofwork.UnitOfWorkContext;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FooBean {

    @UnitOfWork
    public void bar(){

    }

    @UnitOfWork
    public boolean isContextActive(){

        return UnitOfWorkContext.isCurrentlyActive();

    }

}
